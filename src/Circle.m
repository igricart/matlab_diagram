classdef Circle < handle
    %CIRCLE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        txt
        radius
        pos
        offset
        ports
        portGain
        portOffset
        
        fontSize
        fontColor
        backgroundColor
        borderColor
        borderWidth
        borderLineStyle
    end
    
    methods
        function obj = Circle(varargin)            
            if nargin==0
                CircleNormalConstructor(obj, varargin{:});
            elseif ~isa(varargin{1},'Circle')
                CircleNormalConstructor(obj, varargin{:});
            else
                CircleCopyConstructor(obj, varargin{:});
            end
        end
        
        function obj = CircleNormalConstructor(obj, varargin)
            
            global diagram;
            
            if nargin > 1
                obj.txt = varargin{1};
            else
                obj.txt = diagram.circleTxt;
            end
            
            if nargin > 2
                obj.pos = varargin{2};
            else
                obj.pos = diagram.circlePos;
            end
            
            if nargin > 3
                obj.radius = varargin{3};
            else
                obj.radius = diagram.circleRadius;
            end
            
            if nargin > 4
                obj.offset = varargin{4};
            else
                obj.offset = diagram.circleOffset;
            end
            
            if nargin > 5
                inPorts = varargin{5};
                s = size(inPorts);
                for i = 1:s(2)
                    obj.ports = [obj.ports Port(inPorts{i})];
                end
            end
            
            if nargin > 6
                obj.portGain = varargin{6};
            else
                obj.portGain = diagram.circlePortGain;
            end
            
            if nargin > 7
                obj.portOffset = varargin{7};
            else
                obj.portOffset = diagram.circlePortOffset;
            end
            
            obj.SetPos(obj.pos);
            
            diagram.circles{end+1} = obj;            
            obj.fontSize = diagram.circleFontSize;
            obj.fontColor = diagram.circleFontColor;
            obj.backgroundColor = diagram.circleBackgroundColor;
            obj.borderColor = diagram.circleBorderColor;
            obj.borderWidth = diagram.circleBorderWidth;
            obj.borderLineStyle = diagram.circleBorderLineStyle;
        end
        
        function obj = CircleCopyConstructor(obj,obj2)
            
            obj.txt = obj2.txt;
            obj.pos = obj2.pos;
            obj.radius = obj2.radius;
            obj.offset = obj2.offset;
            obj.portGain = obj2.portGain;
            obj.portOffset = obj2.portOffset;
            
            s = size(obj2.ports);
            for i = 1:s(2)
                obj.ports = [obj.ports Port(obj2.ports(i))];
            end
            
            global diagram;
            diagram.circles{end+1} = obj;
            
            obj.fontSize = obj2.fontSize;
            obj.fontColor = obj2.fontColor;
            obj.backgroundColor = obj2.backgroundColor;
            obj.borderColor = obj2.borderColor;
            obj.borderWidth = obj2.borderWidth;
            obj.borderLineStyle = obj2.borderLineStyle;
        end
        
        function SetPos(obj, pos)
            obj.pos = pos;
            
            s = size(obj.ports);
            for i = 1:s(2)
                % set port pos
                ang = (i - 1)*2*pi/s(2) + obj.offset;
                dir = [cos(ang); sin(ang)];
                gain = 1;
                p = obj.pos + gain*obj.radius*dir.';
                obj.ports(i).pos = [p ang];
                
                % set port shiftTxt
                ang = ang + obj.portOffset;
                dir = [cos(ang); sin(ang)];
                gain = obj.portGain;
                p = obj.pos + gain*obj.radius*dir.';
                obj.ports(i).shiftTxt = p(1:2) - obj.ports(i).pos(1:2);
            end
        end
        
        function port = AddPort(obj, txt)
            port = Port(txt);
            obj.ports = [obj.ports port];
            obj.SetPos(obj.pos);
        end
        
        function PrintCircle(obj)
            
            %Initialization
            rectPrintProperties = {'EdgeColor','FaceColor','LineStyle','LineWidth'};
            rectPrintPropertiesValues = {obj.borderColor,obj.backgroundColor,obj.borderLineStyle,obj.borderWidth};
            
            d = obj.radius*2;
            px = obj.pos(1) - obj.radius;
            py = obj.pos(2) - obj.radius;
            c = rectangle('Position',[px py d d],'Curvature',[1,1]);
            set(c,rectPrintProperties,rectPrintPropertiesValues);   
        end

        function PrintText(obj)
            yLimits = get(gca,'YLim');
            
            t = text(obj.pos(1), obj.pos(2), obj.txt, 'Interpreter', 'latex', 'Clipping', 'on', 'FontUnits', 'normalized');
            t.HorizontalAlignment = 'center';
            t.VerticalAlignment = 'middle';
            t.FontSize = obj.fontSize/(yLimits*[-1; 1]);
            t.Color = obj.fontColor;
            
            s = size(obj.ports);
            for i = 1:s(2)
                obj.ports(i).Print();
            end
        end
    end
    
end

