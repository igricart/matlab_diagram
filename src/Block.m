classdef Block < handle
    %BLOCK Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        pos
        size
        txt
        left
        right
        down
        up
        
        fontSize
        fontColor
        backgroundColor
        borderColor
        borderWidth
        borderCurvature
        borderLineStyle
    end
    
    methods
        function obj = Block(varargin)
            if nargin==0
                BlockNormalConstructor(obj, varargin{:});
            elseif ~isa(varargin{1},'Block')
                BlockNormalConstructor(obj, varargin{:});
            else
                BlockCopyConstructor(obj, varargin{:});
            end
        end
        
        function obj = BlockNormalConstructor(obj, varargin)
            global diagram;
            
            if nargin > 1
                obj.txt = varargin{1};
            else
                obj.txt = diagram.blockTxt;
            end
            
            if nargin > 2
                obj.pos = varargin{2};
            else
                obj.pos = diagram.blockPos;
            end
            
            if nargin > 3
                obj.size = varargin{3};
            else
                obj.size = diagram.blockSize;
            end
            
            if nargin > 4
                s = size(varargin{4});
                for i = 1:s(2)
                    obj.left = [obj.left Port(varargin{4}(i))];
                    obj.left(i).shiftTxt = [0.02 0];
                    obj.left(i).horizontalAlignment = 'left';
                    obj.left(i).verticalAlignment = 'middle';
                end
            end
            if nargin > 5
                s = size(varargin{5});
                for i = 1:s(2)
                    obj.right = [obj.right Port(varargin{5}(i))];
                    obj.right(i).shiftTxt = [-0.02 0];
                    obj.right(i).horizontalAlignment = 'right';
                    obj.right(i).verticalAlignment = 'middle';
                end
            end
            if nargin > 6
                s = size(varargin{6});
                for i = 1:s(2)
                    obj.down = [obj.down Port(varargin{6}(i))];
                    obj.down(i).horizontalAlignment = 'center';
                    obj.down(i).verticalAlignment = 'bottom';
                end
            end
            if nargin > 7
                s = size(varargin{7});
                for i = 1:s(2)
                    obj.up = [obj.up Port(varargin{7}(i))];
                    obj.up(i).horizontalAlignment = 'center';
                    obj.up(i).verticalAlignment = 'top';
                end
            end
            
            obj.SetPos(obj.pos);            
            
            diagram.blocks{end+1} = obj;            
            obj.fontSize = diagram.blockFontSize;
            obj.fontColor = diagram.blockFontColor;
            obj.backgroundColor = diagram.blockBackgroundColor;
            obj.borderColor = diagram.blockBorderColor;
            obj.borderWidth = diagram.blockBorderWidth;
            obj.borderCurvature = diagram.blockBorderCurvature;
            obj.borderLineStyle = diagram.blockBorderLineStyle;
        end
        
        function obj = BlockCopyConstructor(obj, obj2)
            
            obj.txt = obj2.txt;
            obj.pos = obj2.pos;
            obj.size = obj2.size;
            
            s = size(obj2.left);
            for i = 1:s(2)
                obj.left = [obj.left Port(obj2.left(i))];
            end
            s = size(obj2.right);
            for i = 1:s(2)
                obj.right = [obj.right Port(obj2.right(i))];
            end
            s = size(obj2.down);
            for i = 1:s(2)
                obj.down = [obj.down Port(obj2.down(i))];
            end
            s = size(obj2.up);
            for i = 1:s(2)
                obj.up = [obj.up Port(obj2.up(i))];
            end
            
            global diagram;
            diagram.blocks{end+1} = obj;
            
            obj.fontSize = obj2.fontSize;
            obj.fontColor = obj2.fontColor;
            obj.backgroundColor = obj2.backgroundColor;
            obj.borderColor = obj2.borderColor;
            obj.borderWidth = obj2.borderWidth;
            obj.borderCurvature = obj2.borderCurvature;
            obj.borderLineStyle = obj2.borderLineStyle;
        end
        
        function SetPos(obj, pos)
            obj.pos = pos;
            
            s = size(obj.left);
            for i = 1:s(2)
                p = obj.pos + obj.size*[-1 0; 0 1]/2 - [0 i*obj.size(2)/(s(2)+1)];
                ang = pi;
                obj.left(i).pos = [p ang];
            end
            
            s = size(obj.right);
            for i = 1:s(2)
                p = obj.pos + obj.size*[1 0; 0 1]/2 - [0 i*obj.size(2)/(s(2)+1)];
                ang = 0;
                obj.right(i).pos = [p ang];
            end
            
            s = size(obj.down);
            for i = 1:s(2)
                p = obj.pos + obj.size*[-1 0; 0 -1]/2 + [i*obj.size(1)/(s(2)+1) 0];
                ang = 3*pi/2;
                obj.down(i).pos = [p ang];
            end
            
            s = size(obj.up);
            for i = 1:s(2)
                p = obj.pos + obj.size*[-1 0; 0 1]/2 + [i*obj.size(1)/(s(2)+1) 0];
                ang = pi/2;
                obj.up(i).pos = [p ang];
            end
        end
        
        function SetRelativePosSize(obj, srcBlock, srcPort, targetPort, relativePos)
            sourceDir = '';
            sSrcLeft = size(srcBlock.left);
            for i = 1:sSrcLeft(2)
                if srcBlock.left(i) == srcPort
                    sourceDir = 'left';
                end
            end
            sSrcRight = size(srcBlock.right);
            for i = 1:sSrcRight(2)
                if srcBlock.right(i) == srcPort
                    sourceDir = 'right';
                end
            end
            sSrcDown = size(srcBlock.down);
            for i = 1:sSrcDown(2)
                if srcBlock.down(i) == srcPort
                    sourceDir = 'down';
                end
            end
            sSrcUp = size(srcBlock.up);
            for i = 1:sSrcUp(2)
                if srcBlock.up(i) == srcPort
                    sourceDir = 'up';
                end
            end
            
            targetDir = '';
            sTargetLeft = size(obj.left);
            for i = 1:sTargetLeft(2)
                if obj.left(i) == targetPort
                    targetDir = 'left';
                    targetPortNumber = i;
                end
            end
            sTargetRight = size(obj.right);
            for i = 1:sTargetRight(2)
                if obj.right(i) == targetPort
                    targetDir = 'right';
                    targetPortNumber = i;
                end
            end
            sTargetDown = size(obj.down);
            for i = 1:sTargetDown(2)
                if obj.down(i) == targetPort
                    targetDir = 'down';
                    targetPortNumber = i;
                end
            end
            sTargetUp = size(obj.up);
            for i = 1:sTargetUp(2)
                if obj.up(i) == targetPort
                    targetDir = 'up';
                    targetPortNumber = i;
                end
            end
            
            if strcmp(targetDir, 'left') && strcmp(sourceDir, 'right')
                spacing = srcBlock.size(2)/(sSrcRight(2) + 1);
                obj.size(2) = spacing*(sTargetLeft(2) + 1);
                obj.pos = srcPort.pos(1:2) + relativePos + [0 spacing*targetPortNumber] + [obj.size(1) -obj.size(2)]/2;
            elseif strcmp(targetDir, 'right') && strcmp(sourceDir, 'left')
                spacing = srcBlock.size(2)/(sSrcLeft(2) + 1);
                obj.size(2) = spacing*(sTargetRight(2) + 1);
                obj.pos = srcPort.pos(1:2) + relativePos + [0 spacing*targetPortNumber] + [-obj.size(1) -obj.size(2)]/2;
            elseif strcmp(targetDir, 'down') && strcmp(sourceDir, 'up')
                spacing = srcBlock.size(1)/(sSrcUp(2) + 1);
                obj.size(1) = spacing*(sTargetDown(2) + 1);
                obj.pos = srcPort.pos(1:2) + relativePos + [-spacing*targetPortNumber 0] + [obj.size(1) obj.size(2)]/2;
            elseif strcmp(targetDir, 'up') && strcmp(sourceDir, 'down')
                spacing = srcBlock.size(1)/(sSrcDown(2) + 1);
                obj.size(1) = spacing*(sTargetUp(2) + 1);
                obj.pos = srcPort.pos(1:2) + relativePos + [-spacing*targetPortNumber 0] + [obj.size(1) -obj.size(2)]/2;
            else
                fprintf('Ports cant be aligned\n');
                return;
            end
            
            obj.SetPos(obj.pos);
        end
        
        function port = AddPortLeft(obj, txt)
            port = Port(txt);
            port.shiftTxt = [0.02 0];
            port.horizontalAlignment = 'left';
            port.verticalAlignment = 'middle';
            obj.left = [obj.left port];
            obj.SetPos(obj.pos);
        end
        
        function port = AddPortRight(obj, txt)
            port = Port(txt);
            port.shiftTxt = [-0.02 0];
            port.horizontalAlignment = 'right';
            port.verticalAlignment = 'middle';
            obj.right = [obj.right port];
            obj.SetPos(obj.pos);
        end
        
        function port = AddPortDown(obj, txt)
            port = Port(txt);
            port.horizontalAlignment = 'center';
            port.verticalAlignment = 'bottom';
            obj.down = [obj.down port];
            obj.SetPos(obj.pos);
        end
        
        function port = AddPortUp(obj, txt)
            port = Port(txt);
            port.horizontalAlignment = 'center';
            port.verticalAlignment = 'top';
            obj.up = [obj.up port];
            obj.SetPos(obj.pos);
        end
        
        function PrintBlock(obj)
            
            %Initialization
            rectPrintProperties = {'Curvature','EdgeColor','FaceColor','LineStyle','LineWidth'};
            rectPrintPropertiesValues = {obj.borderCurvature,obj.borderColor,obj.backgroundColor,obj.borderLineStyle,obj.borderWidth};
            
            rectPos = [(obj.pos - obj.size/2) obj.size];
            r = rectangle('Position', rectPos);
            set(r,rectPrintProperties,rectPrintPropertiesValues);
        end

        % Prints all block texts (block name in the center and port names)
        function PrintText(obj)
            
            yLimits = get(gca,'YLim');
            
            t = text(obj.pos(1), obj.pos(2), obj.txt, 'Interpreter', 'latex', 'Clipping', 'on', 'FontUnits', 'normalized');
            t.HorizontalAlignment = 'center';
            t.VerticalAlignment = 'middle';
            t.FontSize = obj.fontSize/(yLimits*[-1; 1]);
            t.Color = obj.fontColor;
            
            % Prints names of all left ports.
            s = size(obj.left);
            for i = 1:s(2)
                obj.left(i).Print();
            end
            
            % Prints names of all right ports.
            s = size(obj.right);
            for i = 1:s(2)
                obj.right(i).Print();
            end
            
            % Prints names of all down ports.
            s = size(obj.down);
            for i = 1:s(2)
                obj.down(i).Print();
            end
            
            % Prints names of all up ports.
            s = size(obj.up);
            for i = 1:s(2)
                obj.up(i).Print();
            end
        end
    end
    
end

