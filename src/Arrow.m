classdef Arrow < handle
    %ARROW Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        points
        
        isSmooth
        headBase
        headHeight
        useExtraPoints
        extraPointsDistance
        hasSourceHead %draw arrow head in source point?
        hasTargetHead %draw arrow head in target point?
        useSourceAng
        useTargetAng
        
        useSegmentCenter
        filletRadius
        filletResolution        
        lineColor        
        headBackgroundColor
        headFaceAlpha
        lineStyle
        lineWidth
        lineMarker
        lineMarkerSize
        lineMarkerEdgeColor
        lineMarkerFaceColor
    end
    
    methods
        function obj = Arrow(varargin)
            if ~isa(varargin{1},'Arrow')
                ArrowNormalConstructor(obj,varargin{:});
            else
                ArrowCopyConstructor(obj,varargin{:});
            end
        end
        
        function obj = ArrowNormalConstructor(obj,varargin)
            global diagram;
            diagram.arrows{end+1} = obj;

            obj.isSmooth = diagram.arrowIsSmooth;
            obj.headBase = diagram.arrowHeadBase;
            obj.headHeight = diagram.arrowHeadHeight;
            obj.useExtraPoints = diagram.arrowUseExtraPoints;
            obj.extraPointsDistance = diagram.arrowExtraPointsDistance;
            obj.hasSourceHead = diagram.arrowHasSourceHead;
            obj.hasTargetHead = diagram.arrowHasTargetHead;
            obj.useSourceAng = diagram.arrowUseSourceAng;
            obj.useTargetAng = diagram.arrowUseTargetAng;            
            obj.useSegmentCenter = diagram.arrowUseSegmentCenter;
            obj.filletRadius = diagram.arrowFilletRadius; 
            obj.filletResolution = diagram.arrowFilletResolution;
            
            for i = 1:(nargin-1)
                obj.points{i} = varargin{i};
            end
            
            obj.lineColor = diagram.arrowLineColor;
            obj.headBackgroundColor = diagram.arrowHeadBackgroundColor;
            obj.headFaceAlpha = diagram.arrowHeadFaceAlpha;
            obj.lineStyle = diagram.arrowLineStyle;
            obj.lineWidth = diagram.arrowLineWidth;
            obj.lineMarker = diagram.arrowLineMarker;
            obj.lineMarkerSize = diagram.arrowLineMarkerSize;
            obj.lineMarkerEdgeColor = diagram.arrowLineMarkerEdgeColor;
            obj.lineMarkerFaceColor = diagram.arrowLineMarkerFaceColor;
        end
        
        function obj = ArrowCopyConstructor(obj,obj2)
            global diagram;
            diagram.arrows{end+1} = obj;
            
            obj.points = obj2.points;
            obj.isSmooth = obj2.isSmooth;
            obj.headBase = obj2.headBase;
            obj.headHeight = obj2.headHeight;
            obj.useExtraPoints = obj2.useExtraPoints;
            obj.extraPointsDistance = obj2.extraPointsDistance;
            obj.hasSourceHead = obj2.hasSourceHead;
            obj.hasTargetHead = obj2.hasTargetHead;
            obj.useSourceAng = obj2.useSourceAng;
            obj.useTargetAng = obj2.useTargetAng;
            obj.useSegmentCenter = obj2.useSegmentCenter;
            obj.filletRadius = obj2.filletRadius;
            obj.filletResolution = obj2.filletResolution;
            
            obj.lineColor = obj2.lineColor;
            obj.headBackgroundColor = obj2.headBackgroundColor;
            obj.headFaceAlpha = obj2.headFaceAlpha;
            obj.lineStyle = obj2.lineStyle;
            obj.lineWidth = obj2.lineWidth;
            obj.lineMarker = obj2.lineMarker;
            obj.lineMarkerSize = obj2.lineMarkerSize;
            obj.lineMarkerEdgeColor = obj2.lineMarkerEdgeColor;
            obj.lineMarkerFaceColor = obj2.lineMarkerFaceColor;
        end
        
        function PrintArrowHead(obj, pos, ang)            
            patchPrintProperties = {'FaceColor','FaceAlpha','EdgeColor'};
            patchPrintPropertiesValues = {obj.headBackgroundColor,obj.headFaceAlpha,obj.lineColor};            
            p1 = [-obj.headHeight obj.headBase]';
            p2 = [-obj.headHeight -obj.headBase]';
            R = [cos(ang) -sin(ang); sin(ang) cos(ang)];
            p1 = R*p1;
            p2 = R*p2;
            x = [0 p1(1) p2(1)] + pos(1);
            y = [0 p1(2) p2(2)] + pos(2);
            p = patch(x,y,obj.headBackgroundColor);
            set(p,patchPrintProperties,patchPrintPropertiesValues);
        end
        
        function PrintArrowHeads(obj)
            %Print source Arrow
            if obj.hasSourceHead
                if obj.useSourceAng
                    p = obj.points{1};
                    obj.PrintArrowHead(p(1:2), p(3) + pi);
                else
                    p1 = obj.points{1}(1:2);
                    p2 = obj.points{2}(1:2);
                    p = p1 - p2;
                    obj.PrintArrowHead(p1, atan2(p(2), p(1)));
                end
            end
            
            %Print Target Arrow
            if obj.hasTargetHead
                if obj.useTargetAng
                    p = obj.points{end};
                    obj.PrintArrowHead(p(1:2), p(3) + pi);
                else
                    p1 = obj.points{end}(1:2);
                    p2 = obj.points{end-1}(1:2);
                    p = p1 - p2;
                    obj.PrintArrowHead(p1, atan2(p(2), p(1)));
                end
            end            
        end

        function arrowSpline = MakeArrowSpline(obj,splinePoints,sourceAng,targetAng)
            breaks = 1:length(splinePoints);
            sourceDir = [cos(sourceAng); sin(sourceAng)];
            targetDir = -[cos(targetAng); sin(targetAng)];            
            splinePointsAndConditions = [sourceDir, splinePoints, targetDir]; % spline points with first and end points derivative conditions
            arrowSpline = csape(breaks, splinePointsAndConditions, [1 1 1 1]);
        end
        
        function centerPoints = GetSegmentsCenters(obj,p)
            %Gets the center points of the segments
            s = size(p);
            centerPoints = [];
            for i = 1:(s(2)-1)
                centerPoints{i} = 0.5*(p{i}(1:2) + p{i+1}(1:2));
            end
        end
        
        function AddFillets(obj)
            s = size(obj.points);
            if s(2) <= 2 %no sense of adding fillets with only 2 points
                return;
            end
            
            filletPoints = {};
            for i = 2:(s(2)-1)
                R = obj.filletRadius;
                
                %Get the 3 points
                firstPoint = obj.points{i-1}(1:2);
                secondPoint = obj.points{i}(1:2);
                thirdPoint = obj.points{i+1}(1:2);
                
                %Useful vectors
                vA = (firstPoint - secondPoint);
                lA = norm(vA);
                uA = vA/lA;                
                vB = (thirdPoint - secondPoint);
                lB = norm(vB);
                uB = vB/lB;
                uC = (uA + uB)/norm(uA + uB);
                
                %Angle between the 2 vectors starting from the 2nd point
                cosAng = dot(uA,uB);
                sinAng = abs(uA(1)*uB(2) - uA(2)*uB(1));
                
                %Check if the 3 points are colinear
                if abs(sinAng) < 1e-10
                    continue;
                end
                
                %Check if the radius is OK
                tanHalfAng = sinAng/(1 + cosAng);
                dist = R/tanHalfAng;                
                if (dist > 0.5*lA) || (dist > 0.5*lB)
                    R = min(R, 0.5*min(lA, lB)*tanHalfAng);
                end
                if i == 2 %first two points
                    if (dist > 0.5*(lA - obj.headHeight)) && obj.hasSourceHead
                        R = min(R, 0.5*(lA - obj.headHeight)*tanHalfAng);
                    end
                elseif i == (s(2)-1) %last two points
                    if (dist > 0.5*(lB - obj.headHeight)) && obj.hasTargetHead
                        R = min(R, 0.5*(lB - obj.headHeight)*tanHalfAng);
                    end
                end
                
                %Useful points and vectors
                circleCenter = secondPoint + R*uC*sqrt(2/(1 - cosAng));
                firstArcPoint = secondPoint + R*uA/tanHalfAng;
                lastArcPoint = secondPoint + R*uB/tanHalfAng;
                v1 = firstArcPoint - circleCenter;
                thetaStart = atan2(v1(2),v1(1));
                v2 = lastArcPoint - circleCenter;
                thetaEnd = atan2(v2(2),v2(1));
                
                %Correct angle sweep direction
                if abs(thetaStart - thetaEnd) > pi
                    if sign(thetaEnd) == -1
                        thetaEnd = thetaEnd + 2*pi;
                    elseif sign(thetaStart) == -1
                        thetaStart = thetaStart + 2*pi;
                    end
                end
                
                %Calculate the discrete fillet arc points
                arcAngles = linspace(thetaStart,thetaEnd,obj.filletResolution);
                arcPoints = {};
                for j = 1:length(arcAngles)
                    arcPoints{j} = circleCenter + R*[cos(arcAngles(j)) sin(arcAngles(j))];
                end
                
                %Add fillet points to the set
                filletPoints = [filletPoints arcPoints];
            end
            
            %Modify obj.points
            allPoints = filletPoints;
            if norm(obj.points{1}(1:2) - allPoints{1}) ~= 0
                allPoints = [obj.points{1} allPoints];
            end
            if norm(obj.points{end}(1:2) - allPoints{end}) ~= 0
                allPoints = [allPoints obj.points{end}];
            end
            obj.points = allPoints;            
        end
        
        function PlotPoints(obj,varargin) %For debug only
            s = size(obj.points);
            x = zeros(1,s(2));
            y = zeros(1,s(2));
            for i=1:s(2)
                x(i) = obj.points{i}(1);
                y(i) = obj.points{i}(2);                
            end
            hold on;
            temp = plot(x, y, 'kO');
            
            if isempty(varargin)
                color = 'k';
            else
                color = varargin{1};
            end
            
            set(temp,'MarkerEdgeColor','k','MarkerFaceColor',color);
            hold off;
        end
        
        function Print(obj)
            hold on;
            
            %Initialization
            linePrintProperties = {'Color','LineStyle','LineWidth','Marker','MarkerSize','MarkerEdgeColor','MarkerFaceColor'};
            linePrintPropertiesValues = {obj.lineColor,obj.lineStyle,obj.lineWidth,obj.lineMarker,obj.lineMarkerSize,obj.lineMarkerEdgeColor,obj.lineMarkerFaceColor};
            
            %Get source and target angles
            sourceAng = obj.points{1}(3);            
            if ~obj.useSourceAng
                p1 = obj.points{1}(1:2);
                p2 = obj.points{2}(1:2);
                p = p2 - p1;
                sourceAng = atan2(p(2), p(1));
            end            
            targetAng = obj.points{end}(3);
            if ~obj.useTargetAng                    
                p1 = obj.points{end}(1:2);
                p2 = obj.points{end-1}(1:2);
                p = p2 - p1;
                targetAng = atan2(p(2), p(1));
            end            
            
            %Initialize spline points
            s = size(obj.points);
            splinePoints = zeros(2,s(2));
            for i = 1:s(2)
                splinePoints(:,i) = obj.points{i}(1:2);
            end
                        
            if obj.useExtraPoints || obj.hasSourceHead           
                extraPointSource = obj.points{1}(1:2) + (obj.extraPointsDistance*[cos(sourceAng) -sin(sourceAng); sin(sourceAng) cos(sourceAng)]*[1 0]')';
                splinePoints(:,1) = extraPointSource;
                obj.points = [obj.points(1) extraPointSource obj.points(2:end)];
            end
            
            if obj.useExtraPoints || obj.hasTargetHead
                extraPointTarget = obj.points{end}(1:2) + (obj.extraPointsDistance*[cos(targetAng) -sin(targetAng); sin(targetAng) cos(targetAng)]*[1 0]')';
                splinePoints(:,end) = extraPointTarget;                
                obj.points = [obj.points(1:end-1) extraPointTarget obj.points(end)];
            end            
            
            %Change middle points if corners have fillets
            if (obj.filletRadius~=0) && (~obj.isSmooth)
                obj.AddFillets;
            end
            
            %Print line segments or spline
            if ~obj.isSmooth
                s = size(obj.points);
                printPoints = zeros(2,s(2));
                for i = 1:s(2)
                    printPoints(:,i) = obj.points{i}(1:2);
                end
                temp = plot(printPoints(1,:), printPoints(2,:));
                set(temp,linePrintProperties,linePrintPropertiesValues);
            else
                arrowSpline = obj.MakeArrowSpline(splinePoints,sourceAng,targetAng); %printPoints are now only the "middle" points in case of extra added points
                handles_before = findall(gca); %Little trick to get fnplt handle
                fnplt(arrowSpline);
                fnplt_handle = setdiff(findall(gca), handles_before); %Little trick to get fnplt handle
                set(fnplt_handle, linePrintProperties, linePrintPropertiesValues);
                
                if obj.useExtraPoints || obj.hasSourceHead
                    first2Points = zeros(2);
                    first2Points(:,1) = obj.points{1}(1:2);                    
                    first2Points(:,2) = obj.points{2}(1:2);
                    temp = plot(first2Points(1,:), first2Points(2,:));                    
                    set(temp,linePrintProperties,linePrintPropertiesValues);
                end
                if obj.useExtraPoints || obj.hasTargetHead
                    last2Points = zeros(2);
                    last2Points(:,1) = obj.points{end}(1:2);                    
                    last2Points(:,2) = obj.points{end-1}(1:2);
                    temp = plot(last2Points(1,:), last2Points(2,:));                    
                    set(temp,linePrintProperties,linePrintPropertiesValues);
                end
            end
            
            % Print arrow heads
            obj.PrintArrowHeads();
            
            hold off;
        end
    end
    
    
end

