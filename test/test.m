clear;
clc;

Diagram2.Init();
global diagram;
diagram.blockBorderCurvature = 0.5;
diagram.circleBorderColor = [0 1 0];

%% Blocks
b1 = Block('$\tau^{\mathcal{X}}_{\bar{q}}$', [1.5 0.5], [0.3 0.6], {'$V^{\bar{b}}_{0\bar{b}}$' '$\ddot{\theta}$'}, {'r1' 'r2'}, {'d1' 'd2'}, {'u1' 'u2'});
% b1.backgroundColor = [.7 .8 .9];
% b1.borderColor = [1 0 0];
% b1.borderWidth = 3;
% b1.borderCurvature = .25;
% b1.borderLineStyle = '-.';
% b1.fontSize = 0.2;
% b1.fontColor = [.5 .6 .7];

b5 = Block(b1);
b5.SetPos([0 0.7]);
b5.fontColor = [1 0 0];
b5.borderCurvature = 0;
b5.AddPortLeft('l3');
b5.AddPortDown('d3');
b5.AddPortDown('d4');

b1pl3 = b1.AddPortLeft('l3');
b1pl3.fontSize = 0.1;
b1pl3.fontColor = [0 0 1];
b1.AddPortRight('r3');
b1.AddPortRight('r4');
b1.AddPortDown('d3');
b1.AddPortUp('u3');
b1.AddPortUp('u4');

b5.SetRelativePosSize(b1, b1.left(1), b5.right(1), [-1 0]);
% Arrow(b1.left(1).pos, b5.right(1).pos);
% Arrow(b5.right(2).pos, b1.left(2).pos);

b2 = Block(['not', char(10), 'centered'], [0.1 0.1], [0.4 0.3], {}, {'$V^b_{0b}$' 'r2' 'r3'}, {}, {});

b3 = Block('\parbox[b]{15in}{\centering centered\\ text}', [0 -1], [0.4 0.3]);
b3pr1 = b3.AddPortRight('r1');
b3pr2 = b3.AddPortRight('r2');
b3pr3 = b3.AddPortRight('r3');
b3pl1 = b3.AddPortLeft('l1');
b3pl2 = b3.AddPortLeft('l2');
b3pu1 = b3.AddPortUp('u1');

%\parbox[b]{XXX in}{...}: XXX is the horizontal lenght of the text box (parbox)
randomString = ['\parbox[b]{15 in}{\centering Step Response of ' '$$G(s)$$\\ $$\omega_n=2.47$$' ' and\\' '$$\zeta=' num2str(zeta(1i)) '$$}'];
b4 = Block(randomString, [2 -1], [1.5 0.3], {}, {'r1' 'r2' 'r3'}, {}, {});
b4pl1 = b4.AddPortLeft('l1');
b4pl2 = b4.AddPortLeft('l2');

%% Circles

c1 = Circle('sum', [2.5 0.4], 0.2, 0, {'+' '+' '+' '+'}, 1.5, pi/16);
% c1.backgroundColor = [.7 .8 .9];
% c1.borderColor = [1 0 0];
% c1.borderWidth = 3;
% c1.borderLineStyle = '-.';
% c1.fontSize = 0.2;
% c1.fontColor = [.5 .6 .7];
c1p5 = c1.AddPort('$\xi$');
c1p5.fontSize = 0.2;
c1p5.fontColor = [0 0 1];

c2 = Circle('subtraction', [1 -0.4], 0.2, pi/8, {'-' '-' '-'});

c3 = Circle(c2);
c3.borderColor = [.2 .4 .6];
c3.txt = '';
c3.radius = 0.1;
c3.offset = -0.5;
c3.portGain = 1.6;
% c3.portOffset = 0.2;
c3.SetPos([0 -0.3]);

%% Arrows

a0 = Arrow(b3pr1.pos,b4pl2.pos);
a0.isSmooth = true;
a0.headBackgroundColor = [1 0 0];
% a0.filletRadius = 0.5;

p1 = b1.right(3).pos;
p2 = b2.right(3).pos;
p3 = b1.right(2).pos;

% diagram.arrowUseExtraPoints = 0;
a1 = Arrow(p3, [p3(1)+0.5 p2(2)], b1.down(2).pos);
% a1.useSegmentCenter = true;
% a1.isSmooth = true;
% a1.headBase = 0.1;
% a1.headHeight = 0.2;
% a1.useExtraPoints = false;
% a1.extraPointsDistance = 0.2;
% a1.hasSourceHead = false;
% a1.hasTargetHead = false;
% a1.useSourceAng = true;
% a1.useTargetAng = false;
% a1.lineColor = [0 0 1];
% a1.headBackgroundColor = [1 0 0];
% a1.headFaceAlpha = 0.5;
% a1.lineStyle = '--';
% a1.lineWidth = 2;
% a1.lineMarker = 'o';
% a1.lineMarkerSize = 15;
% a1.lineMarkerEdgeColor = [0.5 .5 1];
% a1.lineMarkerFaceColor = [0 1 0];
a1.filletRadius = 0.1;

a2 = Arrow(b1.left(1).pos, b2.right(1).pos);
a2.hasSourceHead = true;
% a2.isSmooth = true;
a2.filletRadius = 99999;

a3 = Arrow(b1.right(1).pos, c1.ports(1).pos);
a4 = Arrow(a3);
a5 = Arrow(a3);
a6 = Arrow(a3);
a7 = Arrow(a3);
a4.points = {b1.right(1).pos, c1.ports(2).pos};
a5.points = {b1.right(1).pos, c1.ports(3).pos};
a6.points = {b1.right(1).pos, c1.ports(4).pos};
a7.points = {b1.right(1).pos, c1p5.pos};
a7.filletRadius = 0.4;
a7.lineColor = 'r';

a8 = AutoArrow(b2.right(2).pos, b1.left(2).pos,.5,.5,.5,.5,.5);
a8.useSegmentCenter = false;
a8.filletRadius = 0.02;

a9 = AutoArrow(a8);
a9.useSegmentCenter = true;
a9.lineColor = [1 0 0];
a9.filletRadius = 9999;

a10 = AutoArrow(b5.up(2).pos, b1.down(1).pos,0,.5,1);
a10.extraPointsDistance = 0;
% a10.hasSourceHead = true;
a10.isSmooth = true;
% a10.hasTargetHead = false;
% a10.useSegmentCenter = true;

a11 = AutoArrow(b5.up(2).pos, b1.down(1).pos,0,.5,1);
% a11.isSmooth = true;
a11.useSegmentCenter = true;
a11.extraPointsDistance = 0.25;
a11.filletRadius = 0.1;
% a11.hasSourceHead = true;

%% Print
Diagram2.Print();
% a0.PlotPoints('b');
% a1.PlotPoints('r');
% a8.PlotPoints('r');
a10.PlotPoints('b');
a11.PlotPoints('r');